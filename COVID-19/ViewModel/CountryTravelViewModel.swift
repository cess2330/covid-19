//
//  CountryTravelViewModel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct CountryTravelViewModel {
    
    let countryTravelNote : Note

    init(countryTravelNote: Note) {
        self.countryTravelNote = countryTravelNote
    
    }
    
}
