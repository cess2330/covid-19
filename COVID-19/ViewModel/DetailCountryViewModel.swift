//
//  DetailCountryViewModel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct DetailCountryViewModel {
    
    let country : CountryExtended
    let flag : String
    
    init(country: CountryExtended) {
        self.country = country
        self.flag = "https://flagcdn.com/w1280/\(self.country.CountryISO.lowercased()).png"
        print(self.flag)
        
        
    }
    
}
