//
//  DetailViewController.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {
    
    @IBOutlet var detailTableView: UITableView!
    
    var countryReceived : Country?
    var detailCountryViewModel = [DetailCountryViewModel]()
    var countryTestViewModel = [CountryTestViewModel]()
    var countryTravelViewModel = [CountryTravelViewModel]()
    var countryViewModels = [CountryViewModel]()
    let cellId = "topDetailCell"
    let cellIdCountrytest = "countryTestCell"
    let cellIdCountryTravel = "countryTravelCell"
    let cellIdCountryMap = "countryMapCell"
    let cellIdOtherCountries = "otherCountriesTableCell"
    var countryBasic : CountryBasic?
    var descollapse = false
    var numberOfSections : Int?
    var dontShowLastSection = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        Utils.shared.registerCell(tableView:detailTableView,nib: "DetailTopTableViewCell", identifier: cellId)
        Utils.shared.registerCell(tableView:detailTableView,nib: "CountryTestTableViewCell", identifier: cellIdCountrytest)
        Utils.shared.registerCell(tableView:detailTableView,nib: "CountryTravelTableViewCell", identifier: cellIdCountryTravel)
        Utils.shared.registerCell(tableView:detailTableView,nib: "CountryMapTravelTableViewCell", identifier: cellIdCountryMap)
        Utils.shared.registerCell(tableView:detailTableView,nib: "OtherContriesTableViewCell", identifier: cellIdOtherCountries)
        
        detailTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        detailTableView.rowHeight = UITableView.automaticDimension
        
        print(countryReceived!)
        self.navigationItem.title = self.countryReceived?.Country
        fetchData()
    }
    
    
    fileprivate func fetchData() {
        Services.shared.fetchDataByCountry(slugCountry: countryReceived!.Slug, completion: { (countries, error) in
            
            if let err = error {
                print("Failed to fetch countries:", err)
                return
            }
            
            self.detailCountryViewModel = countries!.map({return DetailCountryViewModel(country: $0)})
           
            
        })
            
            
            Services.shared.fetchDataTestByCountry(slugCountry: self.countryReceived!.Slug) { (countryTest, error) in
                if let err = error {
                    print("Failed to fetch countries:", err)
                    return
                }
                
                if countryTest!.count != 0 {
                    let reversedCountryTest = countryTest!.sorted(by: { $0.Date > $1.Date })
                    print(reversedCountryTest)
                    
                    let lastCountryTest = reversedCountryTest[0]
                    
                    var countryTestWithLastElement = [CountryTest]()
                    countryTestWithLastElement.append(lastCountryTest)
                    
                    self.countryTestViewModel = countryTestWithLastElement.map({return CountryTestViewModel(countryTest: $0)})
                
                    print(self.countryTestViewModel)
                } else {
                    
                    var countryTestarray = [CountryTest]()
                    let countryTest = CountryTest(ID: "WithOutTest", Entity: "", ISO: "", Date: "", SourceURL: "", Source: "", CumulativeTotal: 0)
                    countryTestarray.append(countryTest)
                    
                    self.countryTestViewModel = countryTestarray.map({return CountryTestViewModel(countryTest: $0)})
                }
                
                
            }
                
                Services.shared.fetchDataTravelByCountry(slugCountry: self.countryReceived!.Slug) { (countryTravel, error) in
                    if let err = error {
                        print("Failed to fetch countries:", err)
                        return
                    }
                    
                    self.countryBasic = countryTravel!.Country
                    if countryTravel!.Notes.count != 0 {
                        let lastCountryTravelRecomendation = countryTravel!.Notes[0]
                        
                            var countryTravelNotes = [Note]()
                            countryTravelNotes.append(lastCountryTravelRecomendation)
                            
                            self.countryTravelViewModel = countryTravelNotes.map({return CountryTravelViewModel(countryTravelNote: $0)})
                    } else {
                        
                        var countryTravelarray = [Note]()
                        let countryTravel = Note(ID: "WithOutInformation", CountryCode: "", Note: "", Date: "")
                        countryTravelarray.append(countryTravel)
                        
                        self.countryTravelViewModel = countryTravelarray.map({return CountryTravelViewModel(countryTravelNote: $0)})
                    }
                   
                    DispatchQueue.main.async {
                        
                       
                        self.detailTableView.reloadData()
                       // self.detailTableView.reloadSections([0,1,2,3], with: .automatic)
                       
                    }
                    
                    
                }
            
                
                if numberOfSections == 5 {
                    Services.shared.fetchCountries { (count, err) in
                        if let err = err {
                            print("Failed to fetch countries:", err)
                            return
                        }
                        
                        let countries = count!.Countries.shuffled()
                        
                        self.countryViewModels = countries.map({return CountryViewModel(country: $0)})
                        
                        DispatchQueue.main.async {
                          
                            self.detailTableView.reloadData()
                           // self.detailTableView.reloadSections([0,1,2,3,4], with: .automatic)
                            
                        }
                      
                    }
                }
                
        }

}

extension DetailViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 330
        case 1:
            return 220
        case 2:
            return UITableView.automaticDimension
        case 3:
            return 320
        case 4:
            return 100
        default:
            return 50
        }

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return detailCountryViewModel.count
        case 1:
            return countryTestViewModel.count
        case 2:
            return countryTravelViewModel.count
        case 3:
            return 1
        case 4:
            return 1
        default:
            return 1
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! DetailTopTableViewCell
            let detailViewModel = detailCountryViewModel[indexPath.row]
            cell.confirmedCasesLabel.text = "Total cases confirmed: \(self.countryReceived!.TotalConfirmed)"
            cell.totalDeathLabel.text = "Total deaths confirmed: \(self.countryReceived!.TotalDeaths)"
            cell.totalRecoveredLabel.text = "Total recovered confirmed: \(self.countryReceived!.TotalRecovered)"
            cell.detailTopViewModel = detailViewModel
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdCountrytest, for: indexPath) as! CountryTestTableViewCell
            let testViewModel  = countryTestViewModel[indexPath.row]
            cell.countryTestViewModel = testViewModel
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdCountryTravel, for: indexPath) as! CountryTravelTableViewCell
            let travelViewModel  = countryTravelViewModel[indexPath.row]
            cell.delegate = self
            if descollapse {
                cell.showButton.isHidden = true
            }
            cell.countryTravelViewModel = travelViewModel
            return cell
            
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdCountryMap, for: indexPath) as! CountryMapTravelTableViewCell
            if numberOfSections == 4 {
                cell.viewOthersCountries.isHidden = true
            }
            if self.countryBasic != nil {
                   let latDouble = Double(self.countryBasic!.Lat)
                   let lonDouble = Double(self.countryBasic!.Lon)
                   let coordinate = CLLocationCoordinate2D(latitude: latDouble!, longitude: lonDouble!)
                   cell.setMap(coordinate: coordinate, name: countryReceived!.Country)
            }
            
            return cell
            
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdOtherCountries, for: indexPath) as! OtherContriesTableViewCell
            cell.delegate = self
            cell.otherContriess = countryViewModels
            cell.collectionView.reloadData()
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            cell.layoutIfNeeded()
        }
    
    
}

extension DetailViewController : CountryTravelTableViewCellDelegate {
    
    func showDidTap(_ sender: CountryTravelTableViewCell) {
        
        self.descollapse = true
        self.detailTableView.reloadData()
    }
    
    
}

extension DetailViewController : OtherContriesTableViewCellDelegate {
    func countrySelected(country: CountryViewModel) {
        
        
        Navigations.shared.goToDetail(vCToPresent: self, country: country, dontShowLastSection: true, numberOfSections: 4)
    }
    
    
}
