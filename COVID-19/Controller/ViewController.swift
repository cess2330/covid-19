//
//  ViewController.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import UIKit
import EFAutoScrollLabel

class ViewController: UIViewController {
    
    
    @IBOutlet var countriesTableview: UITableView!
    @IBOutlet var containerView: UIView!
    
    var countryViewModels = [CountryViewModel]()
    let cellId = "countriesCell"
    var newsText : String?
    var newsLabel : EFAutoScrollLabel?
    var countryToPass : Country?
    

    override func viewDidLoad() {
        super.viewDidLoad()
    
        Utils.shared.registerCell(tableView:countriesTableview,nib: "ContriesTableViewCell", identifier: cellId)
        newsLabel = EFAutoScrollLabel(frame: CGRect(x: 0, y:0, width: containerView.frame.width, height: containerView.frame.height))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadPressed))
        
        fetchData()
    }

    
    func configureNewsLabel(text : String) {
        
        newsLabel!.backgroundColor = .red
        newsLabel!.textColor = .white
        newsLabel!.font = .boldSystemFont(ofSize: 13)
        newsLabel!.labelSpacing = 30
        newsLabel!.pauseInterval = 0
        newsLabel!.scrollSpeed = 30
        newsLabel!.textAlignment = .left
        newsLabel!.fadeLength = 1
        newsLabel!.scrollDirection = .left
        newsLabel!.text = text
        self.containerView.addSubview(newsLabel!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let text = newsText {
            configureNewsLabel(text: text)
        }
    }
    
    @objc func reloadPressed() {
        fetchData()
    }


    fileprivate func fetchData() {
        Services.shared.fetchCountries { (count, err) in
            if let err = err {
                print("Failed to fetch countries:", err)
                return
            }
            
            let countries = count!.Countries
            
            var shortDate = count!.Global.Date
            let range = shortDate.index(shortDate.startIndex, offsetBy: 10)..<shortDate.endIndex
            shortDate.removeSubrange(range)

            let date = shortDate.convertStringToDate()
            let finalDate = date.convertDateToString()
            
            self.newsText = " Today \(finalDate) COVID- 19 cases in the World.  New Confirmed: \(count!.Global.NewConfirmed), New Deaths: \(count!.Global.NewDeaths), New recoveries: \(count!.Global.NewRecovered)."
            self.countryViewModels = countries.map({return CountryViewModel(country: $0)})
            DispatchQueue.main.async {
                self.countriesTableview.reloadData()
                self.configureNewsLabel(text: self.newsText!)
            }
          
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DETAIL_SEGUE" {
            let vc = segue.destination as! DetailViewController
            vc.countryReceived = self.countryToPass
            vc.numberOfSections = 5
        }
    }
    
 
    
}


extension ViewController : UITableViewDelegate,UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countryViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ContriesTableViewCell
        let countryViewModel = countryViewModels[indexPath.row]
        cell.countryViewModel = countryViewModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.countryToPass = countryViewModels[indexPath.row].country
        self.performSegue(withIdentifier: "DETAIL_SEGUE", sender: self)
    }
    
    
}
