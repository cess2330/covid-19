//
//  ContriesTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import UIKit
import SDWebImage

class ContriesTableViewCell: UITableViewCell {
    
    
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var confirmedLabel: UILabel!
    @IBOutlet var backGroundView: UIView!
    @IBOutlet var countryImageView: UIImageView!
    @IBOutlet var backGroundViewLabel: UIView!
    
    var countryViewModel: CountryViewModel!{
        didSet {
            topLabel.text = countryViewModel.country!.Country
            confirmedLabel.text = "Total confirmed cases: \(countryViewModel.country!.TotalConfirmed)"
            DispatchQueue.main.async(execute: { () -> Void in
                self.countryImageView.sd_setImage(with: URL(string: self.countryViewModel.flag),placeholderImage:UIImage(named: "Flag")!,options: [.refreshCached])
            })
        }
    }
    override  func awakeFromNib() {
        super.awakeFromNib()
        
        backGroundViewLabel.backgroundColor = UIColor.backGroundCellColor.withAlphaComponent(0.7)
        backGroundView.viewStyle(bgColor: .backGroundCellColor, cornerRadius: 5, withShadow: true)
    }
    
    
  

    
}
