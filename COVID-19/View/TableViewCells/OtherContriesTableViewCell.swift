//
//  OtherContriesTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import UIKit

protocol OtherContriesTableViewCellDelegate : class {
    
    func countrySelected(country : CountryViewModel)
}

class OtherContriesTableViewCell: UITableViewCell {
    

    @IBOutlet var collectionView : UICollectionView!
    
    weak var delegate : OtherContriesTableViewCellDelegate?
    
    var otherContriess = [CountryViewModel]()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        Utils.shared.registerCell(collectionView: self.collectionView, nib: "OtherCountriesCollectionViewCell", identifier: "otherCountriesCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}

extension OtherContriesTableViewCell : UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return otherContriess.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "otherCountriesCell", for: indexPath) as! OtherCountriesCollectionViewCell
        
        cell.otherCountryViewModel = otherContriess[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        delegate?.countrySelected(country: otherContriess[indexPath.row])
    }
    
    
    
}
