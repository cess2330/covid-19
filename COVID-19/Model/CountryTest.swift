//
//  Countrytest.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct CountryTest: Codable {
    
    var ID : String
    var Entity : String
    var ISO : String
    var Date : String
    var SourceURL : String
    var Source : String
    var CumulativeTotal : Int
}
