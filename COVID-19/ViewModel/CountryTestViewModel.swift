//
//  CountryTestViewModel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct CountryTestViewModel {
    
    let countryTest : CountryTest

    init(countryTest: CountryTest) {
        self.countryTest = countryTest
    
    }
    
}
