//
//  Countries.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import Foundation

struct Countries : Codable {
    
    var ID : String
    var Message : String
    var Global : Global
    var Countries : [Country]
  
    
    struct Global : Codable {
        var NewConfirmed : Int
        var TotalConfirmed : Int
        var NewDeaths : Int
        var TotalDeaths : Int
        var NewRecovered : Int
        var TotalRecovered : Int
        var Date : String
   
    }
    
}

