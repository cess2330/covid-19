//
//  CountryMapTravelTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import UIKit
import MapKit

class CountryMapTravelTableViewCell: UITableViewCell {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var viewOthersCountries: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }


    func setMap (coordinate : CLLocationCoordinate2D, name: String) {
        let annotation = MKPointAnnotation()
        annotation.title = name
        annotation.coordinate = coordinate
        let span = MKCoordinateSpan.init(latitudeDelta: 10, longitudeDelta: 10)
        let region = MKCoordinateRegion(center: coordinate , span: span)
        mapView.addAnnotation(annotation)
        mapView.setRegion(region, animated: true)
    }
    
}
