//
//  Services.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import Foundation

enum EndPoints: String {
    
case allContries = "summary"
case dataByCountry = "premium/country/data/"
case dataTestByCountry = "premium/country/testing/"
case dataTravelByCountry = "premium/travel/country/"
case simpleCountries = "countries"
    
}

class Services {
    
    static let shared = Services()
    
    var endPoint: EndPoints?
    let accessToken = "5cf9dfd5-3449-485e-b5ae-70a60e997864"
    
    var url: String{
        let baseUrl = "https://api.covid19api.com/"
        let url = baseUrl + endPoint!.rawValue
        return url
    }
    
    
    // MARK: Services
    
    func fetchCountries(completion: @escaping (Countries?, Error?) -> ()) {
        
        endPoint = .allContries
        guard let url = URL(string: url) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch countries:", err)
                return
            }
            
            let response = resp as! HTTPURLResponse
            let status = response.statusCode
            guard (200...299).contains(status) else {
                DispatchQueue.main.async {
                    self.connectionWithServerError(title: "Service Error", message: "Service Temporarily Unavailable")
                }
            return
           
            }
            guard let data = data else { return }
            do {
                let cont = try JSONDecoder().decode(Countries.self, from: data)
                DispatchQueue.main.async {
                    
                    completion(cont, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    
    
    func fetchDataByCountry(slugCountry: String,completion: @escaping ([CountryExtended]?, Error?) -> ()) {
        
        endPoint = .dataByCountry
        
        var urlRequest = self.url
        urlRequest.append(contentsOf: String(slugCountry))
        print(urlRequest)
        
        guard let url = URL(string: urlRequest) else { return }
        var request = URLRequest(url: url)
        request.setValue(accessToken, forHTTPHeaderField: "X-Access-Token")
        URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch data:", err)
                return
            }
            let response = resp as! HTTPURLResponse
            let status = response.statusCode
            guard (200...299).contains(status) else {
                DispatchQueue.main.async {
                    self.connectionWithServerError(title: "Service Error", message: "Service Temporarily Unavailable")
                }
            return
           
            }
            
            guard let data = data else { return }
            do {
                let cont = try JSONDecoder().decode([CountryExtended].self, from: data)
                DispatchQueue.main.async {
                    
                    completion(cont, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    
    func fetchDataTestByCountry(slugCountry: String,completion: @escaping ([CountryTest]?, Error?) -> ()) {
        
        endPoint = .dataTestByCountry
        
        var urlRequest = self.url
        urlRequest.append(contentsOf: String(slugCountry))
        print(urlRequest)
        
        guard let url = URL(string: urlRequest) else { return }
        var request = URLRequest(url: url)
        request.setValue(accessToken, forHTTPHeaderField: "X-Access-Token")
        URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch data:", err)
                return
            }
            let response = resp as! HTTPURLResponse
            let status = response.statusCode
            guard (200...299).contains(status) else {
                DispatchQueue.main.async {
                    self.connectionWithServerError(title: "Service Error", message: "Service Temporarily Unavailable")
                }
            return
           
            }
            
            guard let data = data else { return }
            do {
                let cont = try JSONDecoder().decode([CountryTest].self, from: data)
                DispatchQueue.main.async {
                    
                    completion(cont, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    
    func fetchDataTravelByCountry(slugCountry: String,completion: @escaping (CountryTravel?, Error?) -> ()) {
        
        endPoint = .dataTravelByCountry
        
        var urlRequest = self.url
        urlRequest.append(contentsOf: String(slugCountry))
        print(urlRequest)
        
        guard let url = URL(string: urlRequest) else { return }
        var request = URLRequest(url: url)
        request.setValue(accessToken, forHTTPHeaderField: "X-Access-Token")
        URLSession.shared.dataTask(with: request) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch data:", err)
                return
            }
            let response = resp as! HTTPURLResponse
            let status = response.statusCode
            guard (200...299).contains(status) else {
                DispatchQueue.main.async {
                    self.connectionWithServerError(title: "Service Error", message: "Service Temporarily Unavailable")
                }
            return
           
            }
            
            guard let data = data else { return }
            do {
                let cont = try JSONDecoder().decode(CountryTravel.self, from: data)
                DispatchQueue.main.async {
                    
                    completion(cont, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    
    func fetchSimpleCountries(completion: @escaping ([SimpleCountry]?, Error?) -> ()) {
        
        endPoint = .simpleCountries
        guard let url = URL(string: url) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                completion(nil, err)
                print("Failed to fetch countries:", err)
                return
            }
            
            let response = resp as! HTTPURLResponse
            let status = response.statusCode
            guard (200...299).contains(status) else {
                DispatchQueue.main.async {
                    self.connectionWithServerError(title: "Service Error", message: "Service Temporarily Unavailable")
                }
            return
           
            }
            
            guard let data = data else { return }
            do {
                let cont = try JSONDecoder().decode([SimpleCountry].self, from: data)
                DispatchQueue.main.async {
                    
                    completion(cont, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    
    
}

extension Services {
    
    public func connectionWithServerError(title: String, message: String) {
        let myAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        myAlert.addAction(okAction)
        let sceneDelegate = UIApplication.shared.connectedScenes.first!.delegate as! SceneDelegate
        sceneDelegate.window!.rootViewController?.present(myAlert, animated: true, completion: nil)
    }
    
}
