//
//  CountryViewModel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import Foundation
import UIKit

struct CountryViewModel {
        
    let country : Country?
    let flag : String
    
    init(country: Country) {
        self.country = country
        self.flag = "https://flagcdn.com/w1280/\(self.country!.CountryCode.lowercased()).png"
        
    }
    
}
