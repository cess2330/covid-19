//
//  CountryTestTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import UIKit

class CountryTestTableViewCell: UITableViewCell {

  
    @IBOutlet var testLabel: UILabel!
    @IBOutlet var backGroundView: UIView!
    
    
    
    var countryTestViewModel: CountryTestViewModel!{
        didSet {
            DispatchQueue.main.async(execute: { () -> Void in
                
                if self.countryTestViewModel.countryTest.ID != "WithOutTest" {
                    self.testLabel.text = """
                        Total tests applied: \(self.countryTestViewModel.countryTest.CumulativeTotal)
                        Source: \(self.countryTestViewModel.countryTest.Source)
                    """
                } else {
                    
                    self.testLabel.text = "Without tests information"
                }
                
            })
        }
    }

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backGroundView.viewStyle(bgColor: .backGroundCellColor, cornerRadius: 5, withShadow: true)
    }

  
    
}
