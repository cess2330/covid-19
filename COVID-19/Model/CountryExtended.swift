//
//  CountryExtended.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct CountryExtended: Codable {

    var ID : String
    var CountryISO : String
    var Country : String
    var Continent : String
    var Population : Int
    var PopulationDensity : Double
    var MedianAge : Double
    var Aged65Older : Double
    var Aged70Older : Double
    var ExtremePoverty : Double
    var GdpPerCapita : Double
    var CvdDeathRate : Double
    var DiabetesPrevalence : Double
    var HandwashingFacilities : Double
    var HospitalBedsPerThousand : Double
    var LifeExpectancy : Double
           
}
