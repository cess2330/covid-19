//
//  OtherCountriesCollectionViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import UIKit

class OtherCountriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var backGroundView: UIView!
    @IBOutlet var countryImageView: UIImageView!
    
    
    var otherCountryViewModel: CountryViewModel!{
        didSet {
            DispatchQueue.main.async(execute: { () -> Void in
                self.countryImageView.sd_setImage(with: URL(string: self.otherCountryViewModel.flag),placeholderImage:UIImage(named: "Flag")!,options: [.refreshCached])
            })
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        backGroundView.viewStyle(bgColor: .backGroundCellColor, cornerRadius: 5, withShadow: true)
    }

}
