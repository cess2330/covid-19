//
//  CountryTravel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import Foundation

struct CountryTravel: Codable {
    
    var Country : CountryBasic
    var Notes : [Note]
}

struct Note : Codable {
    var ID : String
    var CountryCode : String
    var Note : String
    var Date : String
}


struct CountryBasic : Codable {
    
    var ID : String
    var Lat : String
    var Lon : String
    
}

struct SimpleCountry : Codable {
    var Country : String
    var Slug : String
    var ISO2 : String
}


