//
//  Navigations.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import Foundation

class Navigations {
    
    static let shared = Navigations()
    
    func goToDetail(vCToPresent : UIViewController, country : CountryViewModel,dontShowLastSection : Bool, numberOfSections : Int) {
           let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
           let vC = storyboard.instantiateViewController(withIdentifier: "DetailVC") as! DetailViewController
            vC.countryReceived = country.country
            vC.dontShowLastSection = dontShowLastSection
            vC.numberOfSections = numberOfSections
           vCToPresent.present(vC, animated: true, completion: nil)
            
    }

}
