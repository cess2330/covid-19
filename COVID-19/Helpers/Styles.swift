import UIKit

extension UIView {
    public enum Radii {
        static let defaultRadius: CGFloat = 5
    }
    
    
    public func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
    
   
    
    
    
   public func roundCorners(corners: UIRectCorner, radius: CGFloat) {
       let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
  
    
    public func viewStyle(bgColor color: UIColor? = nil, cornerRadius radius: CGFloat? = nil, withShadow: Bool) {
        let fancyView = self.layer
        
        fancyView.masksToBounds = withShadow ? false : true
        
        if let color = color {
            self.backgroundColor = color
        }
        
        if let radius = radius {
            fancyView.cornerRadius = radius
        }
        
        if withShadow {
            fancyView.shadowColor = UIColor.black.withAlphaComponent(0.25).cgColor
            fancyView.shadowOffset = CGSize(width: 0, height: 3)
            fancyView.shadowOpacity = 1.0
            fancyView.shadowRadius = 2.0
        }
    }
    
    public func animateButton() {
        let animatedView = self.layer
        UIView.animate(withDuration: 0.1, animations: {
            animatedView.transform = CATransform3DMakeScale(0.8, 0.8, 1)
        }) { _ in
            UIView.animate(withDuration: 0.1, animations: {
                animatedView.transform = CATransform3DMakeScale(1, 1, 1)
            })
        }
    }
    
    public func addParallaxToView(min: CGFloat, max: CGFloat) {
        let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = min
        xMotion.maximumRelativeValue = max
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = min
        yMotion.maximumRelativeValue = max
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion,yMotion]
        self.addMotionEffect(motionEffectGroup)
    }
    
}

extension UITextField {
    public enum Side {
        case left
        case right
    }
    
    public func addImageToTextField(imageName: String, side: Side, widthImage : CGFloat? = nil,heightImage : CGFloat? = nil,padding : CGFloat? = nil) {
        let image = UIImageView(frame: CGRect.init())
        
        let height = heightImage ?? 20
        let width = widthImage ?? 20
        let paddingLeft = padding ?? 15
        
        image.frame = CGRect(x: paddingLeft, y: 0, width: width, height: height)
        image.image = UIImage(named: imageName)!
        
        let container = UIView.init(frame: CGRect.init())
        container.frame = CGRect(x: 0, y: 0, width: 40, height: height)
        container.addSubview(image)
        
        switch side {
        case .left:
            self.leftView = container
            self.leftViewMode = .always
        case .right:
            self.rightView = container
            self.rightViewMode = .always
        }
    }
    
    public func addButtonToTextField(imageName: String, side: Side, button: inout UIButton) {
        button = UIButton(frame: CGRect.init())
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setImage(UIImage(named: imageName)!, for: .normal)
        
        let container = UIView.init(frame: CGRect.init())
        container.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        container.addSubview(button)
        
        switch side {
        case .left:
            self.leftView = container
            self.leftViewMode = .always
        case .right:
            self.rightView = container
            self.rightViewMode = .always
        }
    }
}
