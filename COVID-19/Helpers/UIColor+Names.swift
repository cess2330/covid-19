//
//  UIColor+Names.swift
//  Wafritaze
//
//  Created by Christian León Pérez Serapio on 19/06/20.
//  Copyright © 2020 Cesar Castillo. All rights reserved.
//

import UIKit

extension UIColor {
    static let backGroundCellColor = UIColor(named: "BackgroundCellColor")!
}
