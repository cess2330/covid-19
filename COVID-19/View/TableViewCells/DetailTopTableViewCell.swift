//
//  DetailTopTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 18/02/21.
//

import UIKit

class DetailTopTableViewCell: UITableViewCell {
    
    @IBOutlet var countryImageView: UIImageView!
    @IBOutlet var populationLabel: UILabel!
    @IBOutlet var confirmedCasesLabel: UILabel!
    @IBOutlet var totalDeathLabel: UILabel!
    @IBOutlet var totalRecoveredLabel: UILabel!
    @IBOutlet var sectionTitleLabel: UILabel!
    
    var detailTopViewModel: DetailCountryViewModel!{
        didSet {
            DispatchQueue.main.async(execute: { () -> Void in
                self.populationLabel.text = "Population: \(self.detailTopViewModel.country.Population) people"
                self.sectionTitleLabel.text = "Tests applied in \(self.detailTopViewModel.country.Country)"
                self.countryImageView.sd_setImage(with: URL(string: self.detailTopViewModel.flag),placeholderImage:UIImage(named: "Flag")!,options: [.refreshCached])
            })
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    
}
