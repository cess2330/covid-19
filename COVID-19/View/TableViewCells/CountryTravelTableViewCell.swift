//
//  CountryTravelTableViewCell.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import UIKit

protocol CountryTravelTableViewCellDelegate : class {
    func showDidTap(_ sender: CountryTravelTableViewCell)
}

class CountryTravelTableViewCell: UITableViewCell {
    
    
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var travelDescriptionTextView: UITextView!
    @IBOutlet var showButton: UIButton!
    
    weak var delegate: CountryTravelTableViewCellDelegate?

    var countryTravelViewModel: CountryTravelViewModel!{
        didSet {
            DispatchQueue.main.async(execute: { () -> Void in
                
                if self.countryTravelViewModel.countryTravelNote.ID != "WithOutInformation" {
                    var shortDate = self.countryTravelViewModel.countryTravelNote.Date
                    let range = shortDate.index(shortDate.startIndex, offsetBy: 10)..<shortDate.endIndex
                    shortDate.removeSubrange(range)

                    let date = shortDate.convertStringToDate()
                    let finalDate = date.convertDateToString()
                    
                        self.dateLabel.text = "Update date: \(finalDate)"
                        self.travelDescriptionTextView.text = "Recomendations: \(self.countryTravelViewModel.countryTravelNote.Note)"
                    
                } else {
                    
                    self.showButton.isHidden = true
                    self.dateLabel.textAlignment = .center
                    self.dateLabel.text = "Without travel information"
                    
                }
                
            })
        }
    }

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        travelDescriptionTextView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    @IBAction func showButtonPressed(_ sender: UIButton) {
        
        self.delegate?.showDidTap(self)
        
    }

 
}
