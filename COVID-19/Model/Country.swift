//
//  Country.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import Foundation

struct Country : Codable {
    let ID : String
    let Country : String
    let CountryCode : String
    let Slug : String
    let NewConfirmed : Int
    let TotalConfirmed : Int
    let NewDeaths : Int
    let TotalDeaths : Int
    let NewRecovered : Int
    let TotalRecovered : Int
    let Date :  String
    
}
