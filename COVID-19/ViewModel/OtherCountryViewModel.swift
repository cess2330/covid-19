//
//  OtherCountryViewModel.swift
//  COVID-19
//
//  Created by Cesar Castillo on 19/02/21.
//

import Foundation


struct OtherCountryViewModel {
    
    let otherCountry : SimpleCountry
    let flag : String

    init(otherCountry: SimpleCountry) {
        self.otherCountry = otherCountry
        self.flag = "https://flagcdn.com/w1280/\(self.otherCountry.ISO2.lowercased()).png"
    
    }
    
}
