//
//  Utils.swift
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

import Foundation


class Utils {
   
    static let shared = Utils()
    
    func registerCell(tableView: UITableView, nib: String? = nil, identifier: String) {
          tableView.register(UINib(nibName: nib ?? identifier, bundle: nil),forCellReuseIdentifier: identifier)
      }
    func registerCell(collectionView: UICollectionView, nib: String? = nil, identifier: String) {
        collectionView.register(UINib(nibName: nib ?? identifier, bundle: nil),forCellWithReuseIdentifier: identifier)
    }
}

extension String {
    
    func convertStringToDate() -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'"
        let date = dateFormatter.date(from: self)
        return date!
    }
    
   
}

extension Date {
    
    func convertDateToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd'-'MM'-'yyyy'"
        let date = dateFormatter.string(from: self)
        return date
    }
}
