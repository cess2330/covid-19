//
//  TableViewCell.h
//  COVID-19
//
//  Created by Cesar Castillo on 17/02/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
